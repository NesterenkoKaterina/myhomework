function createNewUser() {
    let firstName = prompt("Напишите ваше имя");
    let lastName = prompt("Напишите свою фамилию");
    const newUser = {
        firstName,
        lastName,
        getLogin() {
        return (firstName[0] + lastName).toLowerCase();
        }
    };
    Object.defineProperty(newUser, "firstName", {
        get: function() {
        return firstName;
        },
        set: function(x) {
        firstName = x;
        }
    });
    Object.defineProperty(newUser, "lastName", {
        get: function() {
        return lastName;
        },
        set: function(x) {
        lastName = x;
        }
    });
    return newUser;
    }
    let x = createNewUser();
    console.log(x.firstName);
    console.log(x.getLogin());
    