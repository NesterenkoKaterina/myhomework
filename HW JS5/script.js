function createNewUser() {
    const firstName = prompt("Напишите ваше имя");
    const lastName = prompt("Напишите свою фамилию");
    
    const birthday = prompt("Напишите дату рождения dd.mm.yyyy").split(".");
    const birthdayDate = new Date(
        +birthday[2],
        +birthday[1] - 1,
        +birthday[0]
    );
    const newUser = {
        firstName,
        lastName,
        birthdayDate,
        getLogin() {
        return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
        return (new Date()- birthdayDate).getFullYear();
        },
        getPassword() {
        return (
            this.firstName[0].toUpperCase() +
            this.lastName.toLowerCase() +
            birthdayDate.getFullYear()
        );
        }
    };
    return newUser;
    }
    
    console.log(createNewUser().getAge());
    console.log(createNewUser().getPassword());
    